# CURRENCY CHANGE

Script to hourly check currency rates. Can be run locally:
```
python ~/currencies/USDtoEUR.py USD_EUR 0.005 0.885
```
or within a cron job, e.g.
```
acrontab -e
+ 2 */1 * * 1,2,3,4,5 lxplus ~/currencies/USDtoEUR.py USD_EUR 0.005 0.885
```
