#!/usr/bin/env python

from requests import get
from bs4 import BeautifulSoup
import sys
import os

Nlines	= 5

#def readlastline(f):
#    f.seek(-2, 2)              # Jump to the second last byte.
#    while f.read(1) != b"\n":  # Until EOL is found ...
#        f.seek(-2, 1)          # ... jump back, over the read byte plus one more.
#    return f.read()            # Read all data from this point on.
#
#def readsecondtolastline(f):
#    f.seek(-2, 2)              # Jump to the second last byte.
#    while f.read(1) != b"\n":  # Until EOL is found ...
#        f.seek(-2, 1)          # ... jump back, over the read byte plus one more.
#    return f.read()            # Read all data from this point on.

def get_last_n_lines(file_name, N):
    # Create an empty list to keep the track of last N lines
    list_of_lines = []
    # Open file for reading in binary mode
    with open(file_name, 'rb') as read_obj:
        # Move the cursor to the end of the file
        read_obj.seek(0, os.SEEK_END)
        # Create a buffer to keep the last read line
        buffer = bytearray()
        # Get the current position of pointer i.e eof
        pointer_location = read_obj.tell()
        # Loop till pointer reaches the top of the file
        while pointer_location >= 0:
            # Move the file pointer to the location pointed by pointer_location
            read_obj.seek(pointer_location)
            # Shift pointer location by -1
            pointer_location = pointer_location -1
            # read that byte / character
            new_byte = read_obj.read(1)
            # If the read byte is new line character then it means one line is read
            if new_byte == b'\n':
                # Save the line in list of lines
                list_of_lines.append(buffer.decode()[::-1])
                # If the size of list reaches N, then return the reversed list
                if len(list_of_lines) == N:
                    return list(reversed(list_of_lines))
                # Reinitialize the byte array to save next line
                buffer = bytearray()
            else:
                # If last read character is not eol then add it in buffer
                buffer.extend(new_byte)
        # As file is read completely, if there is still data in buffer, then its first line.
        if len(buffer) > 0:
            list_of_lines.append(buffer.decode()[::-1])
    # return the reversed list
    return list(reversed(list_of_lines))

v1 	= sys.argv[1]
eps 	= sys.argv[2]
thr 	= sys.argv[3] 

currency_page = 'https://free.currconv.com/api/v7/convert?q={}&compact=ultra&apiKey=d8522a1042e16528a6b5'.format(v1)
currency = get(currency_page).text

file_object = open(v1+'.txt', 'a')
file_object.write(currency[4+len(v1):len(currency)-1]+'\n')
file_object.close()

last_n_lines = get_last_n_lines(v1+'.txt',Nlines)
#print last_n_lines
#print last_n_lines[Nlines-2]
last = float(last_n_lines[Nlines-2].rstrip('\n'))
secondtolast = float(last_n_lines[Nlines-3].rstrip('\n'))
thirdtolast = float(last_n_lines[Nlines-4].rstrip('\n'))
fourthtolast = float(last_n_lines[Nlines-5].rstrip('\n'))

print('3rd Last rate:\t'+str(fourthtolast))
print('2nd Last rate:\t'+str(thirdtolast))
print('Last rate:\t'+str(secondtolast))
print('Current rate:\t'+currency[4+len(v1):len(currency)-1])

diff 	= (last-secondtolast)/secondtolast
diff2 	= (last-thirdtolast)/thirdtolast
diff3 	= (last-fourthtolast)/fourthtolast
if diff > float(eps) or diff2 > float(eps) or diff3 > float(eps):
    print('Hey! The change was huge!!!')
if last > float(thr):
    print('Hey! The change was huge!!!')
    print('We are above the threshold of '+str(thr))

